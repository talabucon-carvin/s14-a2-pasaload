let balance = 300.00;
let passedLoad = 0;
let varRetype = 0;
document.getElementById("balanceDisplay").innerHTML = balance;

// clearinput
let clearInput = () => {
	document.getElementById('mobile').value = '';
}

// extract number from input in html
let getMobileNumber = () => {
	varMobile = document.getElementById("mobile").value;

}

//confirmation
let confirmPrompt = () => {
	varRetype = parseInt(prompt(`Your balance is ₱${balance}.00 and chose to pass ₱${passedLoad}.00. Confirm by retyping the mobile number:`));
	console.log(varRetype);
}

// display value
let	displayBalance = () => document.getElementById("balanceDisplay").innerHTML = balance;


// display history and append below the previous
let appendHistory = (tag, element) => {
	tag = document.createElement("li");                 //insert <li></li>
	element = document.createTextNode(`passed ₱${passedLoad}.00 to ${varMobile}`); //insert text element         
	tag.appendChild(element);  //insert element inside tags                            
	document.getElementById("history").appendChild(tag); //insert tag and element to id history as chile element
}



// P10 button
let pass10 = () => {
	getMobileNumber();
	
	if (varMobile < 8170000000 || varMobile > 9900000000) { //not valid mob number
		alert('Enter a valid mobile phone')
	}

	else {; 
	passedLoad = 10; //used in display value in confirmation
	
		console.log(varMobile);
		if (balance < 10.00) { // no load
			console.log(`Insufficient Load`);
			alert('Insufficient Load');
		}
		else {
			confirmPrompt(); 
			if (varRetype != varMobile) { //confirmation fail
				alert('Confirmation failed')
			}

			else {
				balance -= 10.00;  //if all requirements satisfied deduct 10 from balance
				passedLoad = 10;  //for display in transaction
				appendHistory();  // append or push li tag and its text at ul
				displayBalance(); //update
				console.log(balance); //debug
			}
		}
	}
}

// P50 button
let pass50 = () =>  {
	getMobileNumber();
	
	if (varMobile < 8170000000 || varMobile > 9990000000) { //not valid mob number
		alert('Enter a valid mobile phone')
	}

	else {; 
	passedLoad = 50; //used in display value in confirmation
	
		console.log(varMobile);
		if (balance < 50.00) { // no load
			console.log(`Insufficient Load`);
			alert('Insufficient Load');
		}
		else {
			confirmPrompt(); 
			if (varRetype != varMobile) { //confirmation fail
				alert('Confirmation failed')
			}

			else {
				balance -= 50.00;  //if all requirements satisfied deduct 50 from balance
				passedLoad = 50;  //for display in transaction
				appendHistory();  // append or push li tag and its text at ul
				displayBalance();
				console.log(balance); //debug
			}
		}
	}
}

// P100 buttton
let pass100 = () =>  {
	getMobileNumber();
	
	if (varMobile < 8170000000 || varMobile > 9900000000) { //not valid mob number
		alert('Enter a valid mobile phone')
	}

	else {; 
	passedLoad = 100; //used in display value in confirmation
	
		console.log(varMobile);
		if (balance < 100.00) { // no load
			console.log(`Insufficient Load`);
			alert('Insufficient Load');
		}
		else {
			confirmPrompt(); 
			if (varRetype != varMobile) { //confirmation fail
				alert('Confirmation failed')
			}

			else {
				balance -= 100.00;  //if all requirements satisfied deduct 10 from balance
				passedLoad = 100;  //for display in transaction
				appendHistory();  // append or push li tag and its text at ul
				displayBalance();
				console.log(balance); //debug
			}
		}
	}
}